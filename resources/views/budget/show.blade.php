@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Title: </label>
                    <p>{{ $budget->title }}</p>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <p>{{ $budget->description }}</p>
                </div>
                <div class="form-group">
                    <label>UAH</label>
                    <p>{{ $budget->UAH }}</p>
                </div>

                @if(!empty($budget->USD))
                    <div class="form-group">
                        <label>USD</label>
                        <p>{{ $budget->USD }}</p>
                    </div>
                @endif

                <a href="{{ route('budget.edit', ['id' => $budget->id]) }}" class="btn btn-primary" style="float: left; margin-right: 10px;"><i class="glyphicon glyphicon-pencil"></i></a>
                <form action="{{ route('budget.destroy', ['id' => $budget->id]) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                </form>
            </div>
        </div>
    </div>
@endsection