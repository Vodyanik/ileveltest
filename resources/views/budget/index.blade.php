@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-8">
                    <a href="{{ route('budget.create') }}" class="btn btn-success">Create</a>
                </div>
                <div class="col-md-4">
                    <form action="{{ route('budget.index') }}" method="GET">
                        <div class="form-group col-md-6">
                            <label>От: </label>
                            <input type="date" class="form-control" id="date" name="date_from" placeholder="Дата" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>До: </label>
                            <input type="date" class="form-control" id="date" name="date_to" placeholder="Дата" required>
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="form-control btn btn-primary">Get!</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <table class="table">
                    <caption>Budget</caption>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>UAH</th>
                        <th>USD</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($budgets as $budget)
                        <tr>
                            <th>{{ $budget->id }}</th>
                            <td>{{ $budget->title }}</td>
                            <td>{{ $budget->description }}</td>
                            <td>{{ $budget->UAH }}</td>
                            <td>{{ $budget->USD }}</td>
                            <td><a href="{{ route('budget.show', ['id' => $budget->id]) }}"><i class="glyphicon glyphicon-eye-open"></i></a></td>
                            <td><a href="{{ route('budget.edit', ['id' => $budget->id]) }}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                        </tr>
                    @endforeach
                    @if(isset($uah) && isset($usd))
                        <tr>
                            <th>Results</th>
                            <td></td>
                            <td></td>
                            <td><b>{{ $uah }}</b></td>
                            <td><b>{{ $usd }}</b></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                {{ $budgets->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection