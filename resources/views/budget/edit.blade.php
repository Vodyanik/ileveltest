@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('budget.update', ['id' => $budget->id]) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input name="title" type="text" class="form-control" id="title" value="{{ $budget->title }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" rows="3" id="description">
                            {{ $budget->description }}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="UAH">UAH</label>
                        <input name="UAH" type="text" class="form-control" id="UAH" value="{{ $budget->UAH }}">
                    </div>
                    <div class="form-group">
                        <label for="USD">USD</label>
                        <input name="USD" type="text" class="form-control" id="USD" value="{{ $budget->USD }}">
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection