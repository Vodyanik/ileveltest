<?php

namespace App\Criteria;

use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class BudgetCriteria.
 *
 * @package namespace App\Criteria;
 */
class BudgetCriteria implements CriteriaInterface
{
    private $sumByField;
    public function __construct($sumByField = 'UAH')
    {
        $this->sumByField = $sumByField;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('user_id', '=', Auth::id())->sum($this->sumByField);
        return $model;
    }
}
