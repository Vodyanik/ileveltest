<?php

namespace App\Repositories;

use Carbon\Carbon;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Abstractions\Repository;
use App\Contracts\Repositories\BudgetRepository as RepositoryContract;
use App\Models\Budget;

/**
 * Class BudgetRepositoryEloquent
 * @package App\Repositories\Eloquent
 */
class BudgetRepository extends Repository implements RepositoryContract
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Budget::class;
    }


    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param $date_from
     * @param $date_to
     * @return mixed
     */
    public function betweenDate($date_from, $date_to)
    {
        return $this->model->whereBetween('created_at', [
            $date_from, Carbon::parse($date_to)->addDay()->subSecond(),
        ]);
    }
    
}
