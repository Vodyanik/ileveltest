<?php

namespace App\Services;

use App\Abstractions\Service;
use App\Contracts\Services\BudgetService as ServiceContract;
use App\Contracts\Repositories\BudgetRepository as Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Facades\CurrencyConverter;

class BudgetService extends Service implements ServiceContract
{
    /**
     * BudgetService constructor.
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function get(Request $request)
    {
        if (empty($request->except('page'))) {
            return $this->repository->paginate(5);
        }
        $budgets = $this->repository->betweenDate($request->get('date_from'), $request->get('date_to'));
        $uah = $budgets->sum('UAH');
        $usd = $budgets->sum('USD');
        return [
            'budgets' => $budgets->paginate(5),
            'uah' => $uah,
            'usd' => $usd,
        ];
    }

    /**
     * @param Request $request
     * @return mixed|void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $request->request->add([
            'user_id' => Auth::id(),
            'USD' => CurrencyConverter::convertToUSD($request->get('UAH')),
        ]);
        $this->repository->create($request->all());
    }

}