<?php

namespace App\Services;

use GuzzleHttp;

class CurrencyConverter {

    public function convertToUSD($amount)
    {
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', 'https://api.privatbank.ua/p24api/pubinfo', [
            'query' => [
                'exchange' => '',
                'json' => '',
                'coursid' => 11,
            ]
        ]);

        if ($res->getStatusCode() != 200) {
            return null;
        }

        $results = GuzzleHttp\json_decode($res->getBody()->getContents());
        foreach ($results as $result) {
            if ($result->ccy === 'USD') {
                $amountTo = $result->sale;
            }
        }

        return round($amount / $amountTo, 2);
    }

}