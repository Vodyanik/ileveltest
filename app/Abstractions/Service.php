<?php

namespace App\Abstractions;

use Illuminate\Http\Request;
use App\Abstractions\Repository as Repository;
use App\Abstractions\Contracts\Service as ServiceContract;

abstract class Service implements ServiceContract
{
    protected $repository;

    /**
     * Service constructor.
     *
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Return a collection of records
     *
     * @param Request $request
     * @return mixed
     */
    public function get(Request $request)
    {
        return $this->repository->all();
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function show(string $id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * @param Request $request
     * @param string $id
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(Request $request, string $id)
    {
        return $this->repository->update($request->all(), $id);
    }

    /**
     * @param string $id
     * @return int
     */
    public function destroy(string $id)
    {
        return $this->repository->delete($id);
    }
}
