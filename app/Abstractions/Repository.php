<?php

namespace App\Abstractions;

use App\Abstractions\Contracts\Repository as RepositoryContract;
use Prettus\Repository\Eloquent\BaseRepository;

abstract class Repository extends BaseRepository implements RepositoryContract {

}