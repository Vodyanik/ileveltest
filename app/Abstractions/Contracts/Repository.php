<?php

namespace App\Abstractions\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

interface Repository extends RepositoryInterface {

}