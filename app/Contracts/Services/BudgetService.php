<?php

namespace App\Contracts\Services;

use App\Abstractions\Contracts\Service;
use Illuminate\Http\Request;

interface BudgetService extends Service
{
    public function get(Request $request);
}