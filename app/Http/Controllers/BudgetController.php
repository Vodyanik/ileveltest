<?php

namespace App\Http\Controllers;

use App\Services\BudgetService as Service;
use App\Http\Requests\Budget\Index;
use App\Http\Requests\Budget\Store;
use App\Http\Requests\Budget\Update;
use App\Http\Requests\Budget\Destroy;

class BudgetController extends Controller
{
    protected $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @param Index $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Index $request)
    {
        $budgets = $this->service->get($request);
        return view('budget.index',
            is_array($budgets) ? $budgets : ['budgets' => $budgets]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('budget.create');
    }

    /**
     * @param Store $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Store $request)
    {
        $this->service->store($request);
        return redirect('budget');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $budget = $this->service->show($id);
        return view('budget.show', [
            'budget' => $budget,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $budget = $this->service->show($id);
        return view('budget.edit', [
            'budget' => $budget,
        ]);
    }

    /**
     * @param Update $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(Update $request, $id)
    {
        $this->service->update($request, $id);
        return redirect()->route('budget.index');
    }

    /**
     * @param Destroy $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Destroy $request, $id)
    {
        $this->service->destroy($id);
        return redirect('budget');
    }

}
