<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ServicesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Contracts\Services\BudgetService::class, \App\Services\BudgetService::class);
        $this->app->bind(\App\Facades\CurrencyConverter::class, \App\Services\CurrencyConverter::class);
    }
}
