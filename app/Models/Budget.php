<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Budget.
 *
 * @package namespace App\Models;
 */
class Budget extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'UAH',
        'USD',
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
