<?php

use Faker\Generator as Faker;
use App\Models\Budget as Model;

$factory->define(Model::class, function (Faker $faker) {
    $uah = rand(-100, 100);
    return [
        'user_id' => 1,
        'title' => $faker->word,
        'description' => $faker->text,
        'UAH' => $uah,
        'USD' => $uah / 3,
    ];
});
