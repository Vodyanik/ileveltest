<?php

use Illuminate\Database\Seeder;
use App\Models\Budget;

class BudgetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Budget::class, 20)->create();
    }
}
